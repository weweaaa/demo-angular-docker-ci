# Step 1: Build the app in image 'builder'
FROM node:12.8-alpine AS builder

WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN npm run build -- --prod --output-path dist

# # Step 2: Use build output from 'builder'
# FROM nginx:stable-alpine
# LABEL version="1.0"

# COPY nginx.conf /etc/nginx/nginx.conf

# WORKDIR /usr/share/nginx/html
# COPY --from=builder /usr/src/app/dist/demo/ .



# gitlab ci：https://javascript-conference.com/blog/build-test-deployment-angular-gitlab-ci/
# 公開的 Docker angular build：https://hub.docker.com/r/trion/ng-cli-karma/tags
